function Header({ filterFeeds }) {
  return (
    <div className="header-container">
      <input
        type="search"
        placeholder="Please enter keywords"
        onChange={filterFeeds}
      />
    </div>
  );
}

export default Header;
