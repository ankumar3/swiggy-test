import "./Item.css";

function Item({ item }) {
  function addItem() {
    console.log("Item added", item.name);
  }
  return (
    <div className="item-container">
      <div className="left-side">
        <p className="item-name">{item.name}</p>
        <p className="item-price">{item.price}</p>
        <p className="item-description">{item.description}</p>
      </div>
      <div className="right-side">
        <img src={item.cloudinaryImageId} />
        <button onClick={addItem}>ADD</button>
      </div>
    </div>
  );
}

export default Item;
