import { useCallback, useEffect, useRef, useState } from "react";
import Header from "../../components/Header/Header";
import Item from "../../components/Item/Item";
import { fetchData } from "./Home.service";
import { filterData } from "./Home.util";

function Home() {
  const [feed, setFeed] = useState([]);
  const [filteredFeed, setFilteredFeed] = useState([]);
  const [searchActive, setSearchActive] = useState(false);
  const timeout = useRef(null);

  useEffect(() => {
    fetchData()
      .then((data) => {
        setFeed(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const filterFeeds = useCallback(
    (event) => {
      if (timeout.current) {
        return;
      }

      timeout.current = setTimeout(() => {
        const input = event.target;
        const value = input.value;
        const filteredData = filterData(feed, value);
        console.log("filter hit", value, filteredData);
        setFilteredFeed(filteredData);

        if (value) {
          setSearchActive(true);
        } else {
          setSearchActive(false);
        }

        clearTimeout(timeout.current);
        timeout.current = null;
      }, 1000);
    },
    [setSearchActive, setFilteredFeed, feed]
  );

  return (
    <div>
      <Header filterFeeds={filterFeeds} />
      <p className="title">Veg</p>
      {(searchActive ? filteredFeed : feed).map((item, i) => {
        return <Item item={item} key={i} />;
      })}
    </div>
  );
}

export default Home;
