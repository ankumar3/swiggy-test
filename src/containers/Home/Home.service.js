export function fetchData() {
  return fetch(
    "https://run.mocky.io/v3/128675fd-afe3-43fd-9b9a-cf7a0ee511ef"
  ).then((res) => res.json());
}
