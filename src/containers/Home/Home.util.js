export function filterData(feed, value) {
  const filteredData = feed.filter((item) => {
    const name = item.name.toLowerCase();
    if (name.includes(value.toLowerCase())) {
      return true;
    }
    return false;
  });
  return filteredData;
}
